/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function () {
    'use strict';

    angular
            .module('crossOverApp')
            .directive('collapsible', collapsibleFuntion);

    collapsibleFuntion.$inject = ['$timeout'];
    function collapsibleFuntion($timeout) {
        /* implementation details */
        return {
            restrict: 'AE',
            link: function (scope, element) {
                $timeout(function () {
                    $(element).addClass("collapsible");
                    $(element).collapsible({
                        accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
                    });
                }, 1000);
            }
        };

    }

})();
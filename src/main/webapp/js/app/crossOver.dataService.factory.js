/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function () {
    "use strict";
    angular
            .module('crossOverApp')
            .factory('buildInfoDataService', dataservice);

    dataservice.$inject = ['$http'];

    function dataservice($http) {
        return {
            getBuildList: getBuildList
        };

        function getBuildList() {
            return $http.get("/getBuildList")
                    .then(performGetBuildComplete)
                    .catch(performGetBuildFailed);

            function performGetBuildComplete(response) {
                return response.data;
            }

            function performGetBuildFailed(error) {
                console.log('XHR Failed for login.' + error.data);
            }
        }
    }
})();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function () {
    'use strict';
    angular
            .module('crossOverApp')
            .controller('HomePageCtrl', mainFunction);
    
    mainFunction.$inject = ['$scope'];
    
    function mainFunction($scope) {
        var vm = this;
        vm.message = "Hi, This is CROSS|OVER assignment project made for showcase purpose.";
    }

})();

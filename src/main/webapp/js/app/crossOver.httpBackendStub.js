/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



(function () {

    "use strict";
    angular.module('crossOverApp')
            .run(backendCall);

    backendCall.$inject = ['$httpBackend'];

    function backendCall($httpBackend) {
        // BUild List : my data json object
        var buildList = [{
                name: 'Tenrox-R1_1235',
                owner: '',
                timeStarted: ' ',
                state: 'Pending',
                metrics: {},
                build: {},
                unitTest: {},
                functionalTest: {},
                result: {}

            }, {
                name: '432462',
                owner: 'jtuck',
                timeStarted: '4/18/2014 12:12pm',
                state: 'Running',
                metrics: {},
                build: {},
                unitTest: {},
                functionalTest: {},
                result: {}

            }, {
                name: '432461',
                owner: 'samy',
                timeStarted: '4/18/2014 10:53pm',
                state: 'Rejected',
                metrics: {
                    test: 64,
                    maintainability: 53,
                    security: 64,
                    workmanship: 72
                },
                build: {
                    date: "10:46 am - 4/17/2014"
                },
                unitTest: {
                    passed: 142,
                    failed: 10,
                    percentPassed: 73,
                    codeCovered: 76
                },
                functionalTest: {
                    passed: 4321,
                    failed: 2145,
                    percentPassed: 68,
                    codeCovered: 23
                },
                result: {
                    description: "Change Rejected",
                    action: "Metrics Reduction "
                }

            }, {
                name: 'Tenrox-R1_1234',
                owner: ' ',
                timeStarted: '4/17/2014 09:42am',
                state: 'Complete',
                metrics: {
                    test: 64,
                    maintainability: 53,
                    security: 64,
                    workmanship: 72
                },
                build: {
                    date: "10:46 am - 4/17/2014"
                },
                unitTest: {
                    passed: 142,
                    failed: 10,
                    percentPassed: 73,
                    codeCovered: 76
                },
                functionalTest: {
                    passed: 4321,
                    failed: 2145,
                    percentPassed: 68,
                    codeCovered: 23
                },
                result: {
                    description: "Complete",
                    action: "Deploy Ready"
                }

            }, {
                name: '432460',
                owner: 'samy',
                timeStarted: '4/17/2014 07:51am',
                state: 'Rejected',
                metrics: {
                    test: 64,
                    maintainability: 53,
                    security: 64,
                    workmanship: 72
                },
                build: {
                    date: "10:46 am - 4/17/2014"
                },
                unitTest: {
                    passed: 142,
                    failed: 10,
                    percentPassed: 73,
                    codeCovered: 76
                },
                functionalTest: {
                    passed: 4321,
                    failed: 2145,
                    percentPassed: 68,
                    codeCovered: 23
                },
                result: {
                    description: "Change Rejected",
                    action: "Metrics Reduction "
                }

            }, {
                name: '432459',
                owner: 'samy',
                timeStarted: '4/16/2014 06:43pm',
                state: 'Accepted',
                metrics: {
                    test: 64,
                    maintainability: 53,
                    security: 64,
                    workmanship: 72
                },
                build: {
                    date: "10:46 am - 4/17/2014"
                },
                unitTest: {
                    passed: 142,
                    failed: 10,
                    percentPassed: 73,
                    codeCovered: 76
                },
                functionalTest: {
                    passed: 4321,
                    failed: 2145,
                    percentPassed: 68,
                    codeCovered: 23
                },
                result: {
                    description: "Change Accepted",
                    action: "Auto Merged"
                }

            }];

        //mock data call for data objecy
        $httpBackend.whenGET('/getBuildList').respond(buildList);
       
        //Pass through ui-router calls
        $httpBackend.whenGET(/pages\/.*/).passThrough();

    }

})();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function () {
    'use strict';
    angular
            .module('crossOverApp')
            .controller('processesCtrl', mainFunction);

    mainFunction.$inject = ['$scope', 'buildInfoDataService'];

    function mainFunction($scope, buildInfoDataService) {
        var vm = this;
        vm.chartLabels = ["Passed", "Failed"];
           
        getBuildList();
        function getBuildList() {
            return getBuildListDone().then(function (data) {
                if (data) {
                    console.log("Data Call Succesfull.");
                }
            });

        }
        function getBuildListDone() {
            return buildInfoDataService.getBuildList().then(function (data) {
                vm.BuildList = data;
                return vm.BuildList;
            });
        }
    }


})();
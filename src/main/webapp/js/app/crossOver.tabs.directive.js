/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function () {
    'use strict';

    angular
            .module('crossOverApp')
            .directive('tabs', tabsFuntion);

    function tabsFuntion() {
        /* implementation details */
        return {
            restrict: 'AE',
            link: function (scope, element) {
                $(element).addClass("tabs");
                $(element).tabs();
            }
        };

    }

})();
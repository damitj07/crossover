/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function () {
    'use strict';
    angular
            .module('crossOverApp')
            .run(appRun);

    /* @ngInject */
    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(), "/home");
    }

    function getStates() {

        return [
            {
                state: 'home',
                config: {
                    url: '/home',
                    templateUrl: 'pages/landing-page.html',
                    controller: 'HomePageCtrl',
                    controllerAs: 'vm'
                }
            },
            {
                state: 'processes',
                config: {
                    url: '/processes',
                    templateUrl: 'pages/processes-page.html',
                    controller: 'processesCtrl',
                    controllerAs: 'vm'
                }
            }

        ];
    }

})();